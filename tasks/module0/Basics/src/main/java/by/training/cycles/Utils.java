package by.training.cycles;

public class Utils {

    //Task 1
    public static void cyclesTask1() {
        for (int i = 1; i <6; i++) {
            System.out.println(i);
        }
    }

    //Task 2
    public static void cyclesTask2() {
        for (int i = 5; i > 0; i--) {
            System.out.println(i);
        }
    }

    //Task 3
    public static void cyclesTask3() {
        for (int i = 1; i < 11; i++) {
            System.out.println(i + " x 3 = " + i * 3);
        }
    }

    //Task 4
    public static void cyclesTask4() {
        for (int i = 1; i < 101; i++) {
            if (i % 2 == 0)
            System.out.println(i);
        }
    }

    //Task 5
    public static int cyclesTask5() {
        int i = 0;
        int res = 0;
        while ( i < 100) {
            if (i % 2 != 0){
                res = res + i;
            }
            i++;
        }
        return res;
    }

}
