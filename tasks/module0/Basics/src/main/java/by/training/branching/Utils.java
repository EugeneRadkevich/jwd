package by.training.branching;

public class Utils {
    //Task 1
    public static void branchingTask1() {
        if ( 1< 2) {
            System.out.println("7");
        } else {
            System.out.println("8");
        }
    }

    //Task 2
    public static void branchingTask2() {
        if ( 1< 2) {
            System.out.println("yes");
        } else {
            System.out.println("no");
        }
    }

    //Task 3
    public static void branchingTask3(int a) {
        if ( a < 3) {
            System.out.println("yes");
        } else {
            System.out.println("no");
        }
    }

    //Task 4
    public static void branchingTask4(int a, int b) {
        if ( a == b) {
            System.out.println("Числа равны");
        } else {
            System.out.println("Числа НЕ равны");
        }
    }

    //Task 5
    public static void branchingTask5(int a, int b) {
        if ( a < b) {
            System.out.println("Меньшее число: " + a);
        } else {
            System.out.println("Меньшее число: " + b);
        }
    }
}
