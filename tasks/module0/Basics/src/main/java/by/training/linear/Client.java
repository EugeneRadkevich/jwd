package by.training.linear;
import static by.training.linear.Utils.*;

public class Client {

    public static void main(String[] args) {

        //Task 1.1
        System.out.println("Task 1.1");
        System.out.println(sum (3.1f, 4.1f));
        //Task 1.2
        System.out.println("Task 1.2");
        System.out.println(diff(3.2f, 4.1f));
        //Task 1.3
        System.out.println("Task 1.3");
        System.out.println(moult(3.2f, 4.1f));
        //Task 1.4
        System.out.println("Task 1.4");
        div(3.2f, 0f);
        //Task 2
        System.out.println("Task 2");
        System.out.println(fun(3.2f));
        //Task 3
        System.out.println("*******************Task 3**************************");
        System.out.println(fun2(2.3f, 4.1f));
        //Task 4
        System.out.println("*******************Task 4**************************");
        System.out.println(fun3(2.3f, 4.1f, 5.3f));
        //Task 5
        System.out.println("*******************Task 5**************************");
        System.out.println(average(2.3f, 4.1f));
        //Task 6
        System.out.println("*******************Task 6**************************");
        System.out.println(milk(4, 2));
        //Task 7
        System.out.println("*******************Task 7**************************");
        System.out.println(square(2.3f));
        //Task 8
        System.out.println("*******************Task 8*************************");
        System.out.println(formula(2.3f, 4.1f, 3.9));
        //Task 9
        System.out.println("*******************Task 9**************************");
        System.out.println(formula2(2.3f, 4.1f, 5.3f, 2.1f));
        //Task 10
        System.out.println("*******************Task 10**************************");
        System.out.println(formula3(2.3f, 4.1f));
        //Task 11
        System.out.println("*******************Task 11**************************");
        triangle(2.3, 4.1);
        //Task 12
        System.out.println("*******************Task 12**************************");
        dist(2,3.2, 4.1, 5.1);
        //Task 13
        System.out.println("*******************Task 13**************************");
        triangleTops(2,3.2, 4.1, 5.1, 6.1, 4.7);
        //Task 14
        System.out.println("*******************Task 14**************************");
        circle(5.4);
        //Task 15
        System.out.println("*******************Task 15**************************");
        pi4Degrees();
        //Task 16
        System.out.println("*******************Task 16**************************");
        digitNumber (1234);
        //Task 17
        System.out.println("*******************Task 17**************************");
        twoNumber (3.2, 7.1);
        //Task 18
        System.out.println("*******************Task 18**************************");
        cube(2.3);

    }
}
