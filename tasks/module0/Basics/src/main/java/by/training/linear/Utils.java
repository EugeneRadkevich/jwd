package by.training.linear;

public class Utils {

    //Task 1.1
    public static float sum(float x, float y) {
        return x + y;
    }

    //Task 1.2
    public static float diff(float x, float y) {
        return x - y;
    }

    //Task 1.3
    public static float moult(float x, float y) {
        return x * y;
    }

    //Task 1.4
    public static void div(float x, float y) {
        if (y != 0) {
            System.out.println(x / y);
        } else {
            System.out.println("Деление на ноль!");
        }
    }

    //Task 2
    public static float fun(float a) {
        float c = 3 + a;
        return c;
    }

    //Task 3
    public static float fun2(float x, float y) {
        float z = 2 * x + (y-2) * 5;
        return z;
    }

    //Task 4
    public static float fun3(float a, float b, float c) {
        float z = ((a-3) * b/2) + c;
        return z;
    }

    //Task 5
    public static float average (float x, float y) {
        float av = (x + y) / 2;
        return av;
    }

    //Task 6
    public static float milk (int n, int m) {
        float volLarge = ((80 / n) + 12) * m;
        return volLarge;
    }

    //Task 7
    public static float square (float width) {
        float sq = 2 * width * width;
        return sq;
    }

    //Task 8
    public static double formula (double a, double b, double c) {
        double d = (b + Math.sqrt(Math.pow(b, 2) + 4 * a * c))/2 - Math.pow(a, 3) * c + Math.pow(b, -2);
        return d;
    }

    //Task 9
    public static double formula2 (double a, double b, double c, double d) {
        double res = (a * b)/ (c * d) - (a * b - c)/(c * d);
        return res;
    }

    //Task 10
    public static double formula3 (double x, double y) {
        double res = (Math.sin(x) + Math.cos(y)) / (Math.cos(x) + Math.sin(y)) * Math.tan(x * y);
        return res;
    }

    //Task 11
    public static void triangle (double a, double b) {
        double per = a + b + Math.sqrt(Math.pow(a, 2) + Math.pow(b, 2));
        System.out.println("Периметр треугольника:" + per);
        double ar = a * b / 2;
        System.out.println("Площадь треугольника:" + ar);
    }

    //Task 12
    public static void dist (double x1, double y1, double x2, double y2) {
        double dis = Math.sqrt(Math.pow((y1 - y2), 2) + Math.pow((x1 - x2), 2));
        System.out.println("Расстояние между точками:" + dis);
    }

    //Task 13
    public static void triangleTops (double x1, double y1, double x2, double y2, double x3, double y3) {
        double firstSide = Math.sqrt(Math.pow((y1 - y2), 2) + Math.pow((x1 - x2), 2));
        double secondSide = Math.sqrt(Math.pow((y2 - y3), 2) + Math.pow((x2 - x3), 2));
        double thirdSide = Math.sqrt(Math.pow((y1 - y3), 2) + Math.pow((x1 - x3), 2));
        double per = firstSide + secondSide + thirdSide;
        double halfPer = per / 2;
        double ar = Math.sqrt(halfPer * (halfPer - firstSide) * (halfPer - secondSide) * (halfPer - thirdSide));
        System.out.println("Периметр треугольника: " + per);
        System.out.println("Площадь треугольника: " + ar);
    }

    //Task 14
    public static void circle (double r) {
        double per = 2 * r * Math.PI;
        double ar = Math.PI * r * r;
        System.out.println("Длина окружности: " + per);
        System.out.println("Площадь круга: " + ar);
    }

    //Task 15
    public static void pi4Degrees () {
        System.out.println("Первая степень числа Пи: " + Math.PI);
        System.out.println("Вторая степень числа Пи: " + Math.pow(Math.PI, 2));
        System.out.println("Третья степень числа Пи: " + Math.pow(Math.PI, 3));
        System.out.println("Четвертая степень числа Пи: " + Math.pow(Math.PI, 4));
    }

    //Task 16
    public static void digitNumber (double n) {
        int n4 = (int) (n % 1000 % 100 % 10);
        int n3 = (int) ((n / 10) % 100 % 10);
        int n2 = (int) ((n/100) % 10);
        int n1 = (int) (n/1000);
        System.out.println("Число N1: " + n1 + " Число N2: " + n2 + " Число N3: " + n3 + " Число N4: " + n4);
        int moultNus = n1 * n2 * n3 * n4;
        System.out.println("Произведение четырех четырех цифер заданного числа: " + moultNus);
    }

    //Task 17
    public static void twoNumber (double a, double b) {
        double av = (Math.pow(a, 3) + Math.pow(b, 3)) / 2;
        double geometricAv = Math.sqrt(a * b);
        System.out.println("Среднее арифметическое кубов: " + av);
        System.out.println("Среднее геометрическое: " + geometricAv);
    }

    //Task 18
    public static void cube (double a) {
        double arSide = (Math.pow(a, 2));
        double volCube = (Math.pow(a, 3));
        double arCube = 6 * arSide;
        System.out.println("Площадь грани куба: " + arSide);
        System.out.println("Площадь всех граней куба: " + arCube);
        System.out.println("Объем куба: " + volCube);

    }

}

