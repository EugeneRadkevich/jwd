package by.training.branching;

import static by.training.branching.Utils.*;

public class Client {
    public static void main(String[] args) {
        //Task 1
        System.out.println("*****************Task 1**********************");
        branchingTask1();

        //Task 2
        System.out.println("*****************Task 2**********************");
        branchingTask2();

        //Task 3
        System.out.println("*****************Task 3**********************");
        branchingTask3(2);

        //Task 4
        System.out.println("*****************Task 4**********************");
        branchingTask4(4, 4);

        //Task 5
        System.out.println("*****************Task 5**********************");
        branchingTask5(3, 6);
    }

}
