package by.training.cycles;

import static by.training.cycles.Utils.*;

public class Client {

    public static void main(String[] args) {
            //Task 1
            System.out.println("*****************Task 1**********************");
            cyclesTask1 ();
        //Task 2
        System.out.println("*****************Task 2**********************");
        cyclesTask2 ();
        //Task 3
        System.out.println("*****************Task 3**********************");
        cyclesTask3 ();
        //Task 4
        System.out.println("*****************Task 4**********************");
        cyclesTask4 ();
        //Task 5
        System.out.println("*****************Task 5**********************");
        System.out.println(cyclesTask5 ());
    }


}
