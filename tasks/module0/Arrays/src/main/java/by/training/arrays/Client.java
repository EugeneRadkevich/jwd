package by.training.arrays;
import static by.training.arrays.Utils.*;


public class Client {
    public static void main(String[] args) {
        //Task 1
        System.out.println("***********************Task 1***********************");
        int arr[] = generateArrayPositive(10, 50);
        System.out.println("Исходный массив: ");
        printArray(arr);
        System.out.println("Сумма кратныx числу К элементов: ");
        System.out.println(arraysTask1(arr, 4));


        //Task 2
        System.out.println("***********************Task 2***********************");
        //int arr2[] = generateArray(10, 20);
        int arr2[] = {1, 0, 2, 0, 3, 0};
        System.out.println("Исходный массив: ");
        printArray(arr2);
        System.out.println("Массив из индексов нулевых элементов: ");
        printArray(arraysTask2(arr2));

        //Task 3
        System.out.println("***********************Task 3***********************");
        //int arr3[] = generateArray(10, 20);
        int arr3[] = {0, -1, 2, 0, 3, 0};
        System.out.println("Исходный массив: ");
        printArray(arr3);
        arraysTask3(arr3);

        //Task 4
        //System.out.println("***********************Task 4***********************");
        //int arr4[] = generateArray(10, 20);
        //int arr4[] = {1, 2, 3, 4, 5, 6};
        //System.out.println("Исходный массив: ");
        //printArray(arr4);
        //arraysTask4(arr4);

        //Task 5
        System.out.println("***********************Task 5***********************");
        int arr5[] = generateArray(10, 20);
        //int arr3[] = {0, -1, 2, 0, 3, 0};
        System.out.println("Исходный массив: ");
        printArray(arr5);
        printArray(arraysTask5(arr5));

        //Task 6
        System.out.println("***********************Task 6***********************");
        int arr6[] = generateArray(10, 20);
        System.out.println("Исходный массив: ");
        printArray(arr6);
        System.out.println(arraysTask6(arr6));

        //Task 7
        System.out.println("***********************Task 7***********************");
        int arr7[] = generateArray(10, 20);
        System.out.println("Исходный массив: ");
        printArray(arr7);
        System.out.println("Измененный массив: ");
        printArray(arraysTask7(arr7, 10));

        //Task 8
        System.out.println("***********************Task 8***********************");
        int arr8[] = generateArray(10, 20);
        System.out.println("Исходный массив: ");
        printArray(arr8);
        arraysTask8(arr8);

        //Task 9
        System.out.println("***********************Task 9***********************");
        int arr9[] = generateArray(10, 20);
        System.out.println("Исходный массив: ");
        printArray(arr9);
        System.out.println("Измененный массив: ");
        printArray(arraysTask9(arr9));

        //Task 10
        System.out.println("***********************Task 10***********************");
        int arr10[] = generateArrayPositive(10, 10);
        System.out.println("Исходный массив: ");
        printArray(arr10);
        System.out.println("Измененный массив: ");
        printArray(arraysTask10(arr10));

        //Task 13
        System.out.println("***********************Task 13***********************");
        int arr13[] = generateArrayPositive(20, 100);
        System.out.println("Исходный массив: ");
        printArray(arr13);
        System.out.print("Количество элементов кратных M и находящихся на отрезке" +
                " между L и n: ");
        System.out.println(arraysTask13(arr13, 2, 30, 60));

        //Task 14
        System.out.println("***********************Task 14***********************");
        int arr14[] = generateArrayPositive(10, 20);
        System.out.println("Исходный массив: ");
        printArray(arr14);
        System.out.print("max(четные) + min(нечетные): ");
        System.out.println(arraysTask14(arr14));

        //Task 15
        System.out.println("***********************Task 15***********************");
        int arr15[] = generateArrayPositive(10, 20);
        System.out.println("Исходный массив: ");
        printArray(arr15);
        System.out.println("Числа в отрезке от с до d");
        arraysTask15(arr15, 3, 10);


    }
}
