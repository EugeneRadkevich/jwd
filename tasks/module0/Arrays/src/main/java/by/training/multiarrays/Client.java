package by.training.multiarrays;
import static by.training.multiarrays.Utils.*;

public class Client {
    public static void main(String[] args) {
        //Task 1
        System.out.println("***********************Task 1***********************");
        System.out.println("Исходная матрица: ");
        printMArray(multiArraysTask1());

        //Task 2
        System.out.println("***********************Task 2***********************");
        int arr2[][] = generateMArrayPositive(2, 3, 10);
        System.out.println("Исходная матрица: ");
        printMArray(arr2);

        //Task 3
        System.out.println("***********************Task 3***********************");
        int arr3[][] = generateMArrayPositive(2, 3, 10);
        System.out.println("Исходная матрица: ");
        printMArray(arr3);
        System.out.println("Первый столбец матрицы: ");
        for (int i = 0; i < arr3.length; i++) {
            System.out.println(arr3[i][0]);
        }
        System.out.println("Последний столбец матрицы: ");
        for (int i = 0; i < arr3.length; i++) {
            System.out.println(arr3[i][arr3.length]);
        }

        //Task 4
        System.out.println("***********************Task 4***********************");
        int arr4[][] = generateMArrayPositive(3, 3, 10);
        System.out.println("Исходная матрица: ");
        printMArray(arr4);
        System.out.println("Первая строка матрицы: ");
        for (int j = 0; j < arr4.length; j++) {
            System.out.print(arr4[0][j] + "\t");
        }
        System.out.println();
        System.out.println("Последняя строка матрицы: ");
        for (int i = 0; i < arr4.length; i++) {
            if (i == arr4.length - 1) {
                for (int j = 0; j < arr4.length; j++) {
                    System.out.print(arr4[i][j] + "\t");
                }
            }
        }
        System.out.println();


        //Task 5
        System.out.println("***********************Task 5***********************");
        int arr5[][] = generateMArrayPositive(5, 3, 10);
        System.out.println("Исходная матрица: ");
        printMArray(arr5);
        System.out.println("Четные строки матрицы: ");
        for (int i = 0; i <arr5.length ; i++) {
            if (i % 2 == 0 && i != 0){
                for (int j = 0; j < arr5[i].length; j++) {
                    System.out.print(arr5[i][j] + "\t");
                }
                System.out.println();
            }
        }
        System.out.println();

        //Task 6
        /*System.out.println("***********************Task 6***********************");
        int arr6[][] = generateArrayPositive(10, 3, 10);
        System.out.println("Исходная матрица: ");
        printMArray(arr6);

        System.out.println("Нечетные строки матрицы у которых " +
                "первый элемент больше последнего: ");
        for (int i = 0; i <arr6.length ; i++) {
            if (i % 2 != 0 && i != 0){
                for (int j = 0; j < arr6[i].length; j++) {
                    System.out.print(arr6[i][j] + "\t");
                }
                System.out.println();
            }
        }
        System.out.println();*/

        //Task 7
        System.out.println("***********************Task 7***********************");
        int arr7[][] = generateMArray(5, 5, 10);
        System.out.println("Исходная матрица: ");
        printMArray(arr7);
        System.out.print("Сумма модулей отрицательных нечетных элементов: ");
        System.out.println(multiArraysTask7(arr7));

        //Task 8
        System.out.println("***********************Task 8***********************");
        int arr8[][] = generateMArray(3, 4, 10);
        System.out.println("Исходная матрица: ");
        printMArray(arr8);
        System.out.print("Число 7 встречается в массиве: ");
        System.out.println(multiArraysTask8(arr8) + " раз");

        //Task 9
        System.out.println("***********************Task 9***********************");
        int arr9[][] = generateMArray(3, 3, 10);
        System.out.println("Исходная матрица: ");
        printMArray(arr9);
        System.out.print("Главная диаганаль массива: ");
        multiArraysTask9(arr9);

        //Task 10
        System.out.println("***********************Task 10***********************");
        int arr10[][] = generateMArray(4, 5, 10);
        System.out.println("Исходная матрица: ");
        printMArray(arr10);
        multiArraysTask10(arr10, 2, 3);

        //Task 12
        System.out.println("***********************Task 12***********************");
        System.out.println("Требуемая квадратная матрица: ");
        printMArray(multiArraysTask12(10));


    }
}

