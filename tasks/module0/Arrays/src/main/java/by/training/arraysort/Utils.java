package by.training.arraysort;

public class Utils {

    //Task 1
    public static void arraySortTask1(int arrFirst[], int arrSecond[], int k) {
        System.out.println("Совмещенный массив: ");
        for (int i = 0; i <= k; i++) {
            System.out.print(arrFirst[i] + " ");
        }
        for (int i = 0; i < arrSecond.length; i++) {
            System.out.print(arrSecond[i] + " " );
        }
        for (int i = k + 1; i < arrFirst.length; i++) {
            System.out.print(arrFirst[i] + " ");
        }
        System.out.println();
    }

    //Task 4
    public static void bubbleSorting(int arr[]) {
        int outerCounter = 0;
        int innerCounter = 0;
        System.out.println("Selecting Bubble Sorting  is BEGIN: ");
        System.out.println("outerCounter: " + outerCounter);
        System.out.println("innerCounter: " + innerCounter);

        for (int i = arr.length - 1; i > 0; i--) {
            outerCounter++;
            for (int j = 0; j < i; j++) {
                innerCounter++;
                // сравнить парочку, если больше SWAP
                if (arr[j] > arr[j + 1]) {
                    int temp = arr[j];
                    arr[j] = arr[j + 1];
                    arr[j + 1] = temp;
                }

            }
        }
        System.out.println("Selecting Bubble Sorting  is FINISHED: ");
        System.out.println("outerCounter: " + outerCounter);
        System.out.println("innerCounter: " + innerCounter);

    }


    //Task 5
    public static void toSwap(int arr[], int first, int second){
        int temp = arr[first];
        arr[first] = arr[second];
        arr[second] = temp;

    }

    public static int[] selectingSort(int arr[]) {
        int outerCounter = 0;
        int innerCounter = 0;
        System.out.println("Selecting Sorting  is BEGIN: ");
        System.out.println("outerCounter: " + outerCounter);
        System.out.println("innerCounter: " + innerCounter);

        for (int index = 0; index < arr.length; index++) {
            outerCounter++;
            int min_val = arr[index];
            int min_index = index;

            for (int next_index = index + 1; next_index < arr.length; next_index++) {
                // пробежаться и сравнить оставшийся массив
                innerCounter++;
                if (arr[next_index] < min_val) {
                    min_index = next_index;
                    min_val = arr[next_index];
                }
            }
            // SWAP
            if (index != min_index) {
                toSwap(arr, index, min_index);
            }
        }
        System.out.println("Selecting Sorting  is FINISHED: ");
        System.out.println("outerCounter: " + outerCounter);
        System.out.println("innerCounter: " + innerCounter);
        return arr;
    }
}
