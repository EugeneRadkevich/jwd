package by.training.arrays;

public class Utils {
    public static void printArray(int arr[]) {
        for (int i = 0; i < arr.length; i++) {
                // arr[] = {3, 44, 22, 33}
                if (i == 0) {
                    System.out.print("arr[] = {" + arr[i] + ", ");
                } else if (i == arr.length - 1) {
                    System.out.print(arr[i] + "}\n");
                } else {
                    System.out.print(arr[i] + ", ");
                }
        }
    }

    public static int[] generateArrayPositive(int size, int bound) {
        int arr[] = new int[size]; // {0, 0, 0}
        for (int i = 0; i < size; i++) {
            arr[i] = ((int) (Math.random() * bound));
        }
        return arr;
    }

    public static int[] generateArray(int size, int bound) {
        int arr[] = new int[size]; // {0, 0, 0}
        for (int i = 0; i < size; i++) {
            boolean isPositive = (int) (Math.random() * 2) == 1;
            arr[i] = ((int) (Math.random() * bound)) * (isPositive ? 1 : -1);
        }
        return arr;
    }

    public static int findMax(int arr[]) {
        int max = arr[0];
        int index = 0;
        for (int i = 1; i < arr.length; i++) {
            if (arr[i] > max) {
                max = arr[i];
                index = i;
            }

        }
        System.out.println("Max = " + max);
        System.out.println("Индекс Max, i = " + index);
        return max;
    }

    public static int findMin(int arr[]) {
        int min = arr[0];

        for (int i = 1; i < arr.length; i++) {
            if (arr[i] < min) {
                min = arr[i];
            }

        }
        System.out.println("Min = " + min);
        return min;
    }

    //Task 1
    public static int arraysTask1(int arr[], int k) {
        int res = 0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] % k == 0) {
                res = res + arr[i];
            }
        }
        return res;
    }

    //Task 2
    public static int[] arraysTask2(int arr[]) {
        int count = 0;
        int[] arrTemp = new int[arr.length];
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == 0) {
                    for (int j = 0; j < arrTemp.length; j++) {
                    arrTemp[i] = i;
                    count++;
                    break;
                }
            }
        }
        int[] arrNull = new int[count];
        int countJ = 0;
        for (int i = 0; i < arrTemp.length; i++) {
            if (arrTemp[i] != 0) {
                for (int j = countJ; j < count; j++) {
                    arrNull[j] = arrTemp[i];
                    countJ++;
                    break;
                }

            }
        }
        return arrNull;
    }

    //Task 3
    public static void arraysTask3(int arr[]) {
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] != 0) {
                if (arr[i] > 0 ) {
                    System.out.println("Первым встречается положительный элемент");
                } else {
                    System.out.println("Первым встречается отрицательный элемент");
                }
                break;
            }
        }
    }

    //Task 4
    public static void arraysTask4(int arr[]) {
        int max = 0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] > arr[i+1]) {


                System.out.println("Массив не возрастающий");
                break;
            }
            System.out.println("Массив возрастающий");
        }
    }

    //Task 5
    public static int[] arraysTask5(int arr[]) {
        int arrTemp[] = new int[arr.length];
        int count = 0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] % 2 == 0 && arr[i] != 0) {
                arrTemp[i] = arr[i];
                count++;
            }
        }
        int arrEven[] = new int[count];
        int countJ = 0;
        for (int i = 0; i < arrTemp.length; i++) {
            if (arrTemp[i] != 0) {
                for (int j = countJ; j < count; j++) {
                    arrEven[j] = arrTemp[i];
                    countJ++;
                    break;
                }
            }
        }
        return arrEven;
    }

    //Task 6
    public static int arraysTask6(int arr[]) {
        int res = findMax(arr) - findMin(arr);
        return res;
    }

    //Task 7
    public static int[] arraysTask7(int arr[], int z) {
        int count = 0;
        for (int i = 0; i < arr.length ; i++) {
            if ( arr[i] > z) {
                arr[i] = z;
                count++;
            }
        }
        System.out.println("Количество замен: " + count);
        return arr;
    }
    //Task 8
    public static void arraysTask8 (int arr[]) {
        int countNull = 0;
        int countPos = 0;
        int countNeg = 0;
        for (int i = 0; i < arr.length ; i++) {
            if ( arr[i] > 0) {
                countPos++;
            } if (arr[i] < 0) {
                countNeg++;
            } if (arr[i] == 0) {
                countNull++;
            }
        }
        System.out.println("Количество положительных элементов: " + countPos);
        System.out.println("Количество отрицательных элементов: " + countNeg);
        System.out.println("Количество элементов равных нулью: " + countNull);
    }
    //Task 9
    public static int[] arraysTask9 (int arr[]) {
        int tmp = 0;
        int index = 0;
        int max = findMax(arr);
        int min = findMin(arr);
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == max) {
                tmp = arr[i];
                index = i;
                break;
            }
        }
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] == min) {
                arr[i] = tmp;
                arr[index] = min;
                break;
            }
        }
        return arr;
    }

    //Task 10
    public static int[] arraysTask10 (int arr[]) {
        int count = 0;
        int arrTemp[] = new int[arr.length];
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] > i) {
                arrTemp[i] = arr[i];
                count++;
            }
        }
        int countJ = 0;
        int arrRes[] = new int[count];
        for (int i = 0; i < arrTemp.length; i++) {
            if (arrTemp[i] != 0) {
                for (int j = countJ; j < count; j++) {
                    arrRes[j] = arrTemp[i];
                    countJ++;
                    break;
                }
            }
        }
        return arrRes;
    }

    //Task 13
    public static int arraysTask13 (int arr[], int m, int L, int n) {
        int count = 0;
        if (L > n) {
            for (int i = 0; i < arr.length; i++) {
                if (arr[i] % m == 0 && arr[i] < n && arr[i] > L) {
                    count++;
                }
            }
        }
        else {
            System.out.println("Некорректные данные");
        }
        return count;
    }

    //Task 14
    public static int arraysTask14 (int arr[]) {
        int maxEven = arr[2];
        for (int i = 4; i < arr.length; i++) {
            if (i % 2 == 0 && arr[i] > maxEven) {
                maxEven = arr[i];
            }
        }
        int minOdd = arr[1];
        for (int i = 3; i < arr.length; i++) {
            if (i % 2 != 0 && arr[i] < minOdd) {
                minOdd = arr[i];
            }
        }
        return maxEven + minOdd;
    }

    //Task 15
    public static void arraysTask15 (int arr[], int c, int d) {
        if(d > c) {
            for (int i = 0; i < arr.length; i++) {
                if (arr[i] > c && arr[i] < d) {
                    System.out.print(arr[i] + " ");
                }
            }
        }
        System.out.println();
    }

    //Task 6







}
