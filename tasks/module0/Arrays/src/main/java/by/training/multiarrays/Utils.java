package by.training.multiarrays;

public class Utils {
    public static void printMArray(int mArr[][]) {
        for (int i = 0; i < mArr.length; i++) {
            for (int j = 0; j < mArr[i].length; j++) {
                System.out.print(mArr[i][j] + "\t");
            }
            System.out.println();
        }
    }

    public static int[][] generateMArrayPositive(int sizeI, int sizeJ, int bound) {
        int arr[][] = new int[sizeI][sizeJ];
        for (int i = 0; i < sizeI; i++) {
            for (int j = 0; j < sizeJ; j++) {
                arr[i][j] = ((int) (Math.random() * bound));
            }
        }
        return arr;
    }

    public static int[][] generateMArray(int sizeI, int sizeJ, int bound) {
        int arr[][] = new int[sizeI][sizeJ];
        for (int i = 0; i < sizeI; i++) {
            for (int j = 0; j < sizeJ; j++) {
                boolean isPositive = (int) (Math.random() * 2) == 1;
                arr[i][j] = ((int) (Math.random() * bound))* (isPositive ? 1 : -1);
            }
        }
        return arr;
    }

    //Task 1
    public static int[][] multiArraysTask1() {
        int arr1[][] = new int[3][4];
        arr1[0][0] = 1;
        arr1[0][1] = 0;
        arr1[0][2] = 0;
        arr1[0][3] = 0;
        arr1[1][0] = 1;
        arr1[1][1] = 0;
        arr1[1][2] = 0;
        arr1[1][3] = 0;
        arr1[2][0] = 1;
        arr1[2][1] = 0;
        arr1[2][2] = 0;
        arr1[2][3] = 0;
        return arr1;
    }

    //Task 7
    public static int multiArraysTask7(int arr[][]) {
        int res = 0;
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j <arr[i].length ; j++) {
                if(arr[i][j] < 0 && arr[i][j] % 2 != 0) {
                    res = res + Math.abs(arr[i][j]);
                }
            }
        }
        return res;
    }

    //Task 8
    public static int multiArraysTask8(int arr[][]) {
        int count = 0;
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j <arr[i].length ; j++) {
                if(arr[i][j] ==7) {
                    count++;
                }
            }
        }
        return count;
    }

    public static void multiArraysTask9(int arr[][]) {
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j <arr[i].length ; j++) {
                if(i == j) {
                    System.out.print(arr[i][j] + "\t");;
                }
            }
        }
        System.out.println();
    }

    public static void multiArraysTask10(int arr[][], int k, int p) {
        System.out.println("Строка с индексом " + k + ": ");
        for (int i = 0; i < arr.length; i++) {
            if (i == k){
                for (int j = 0; j <arr[i].length ; j++) {
                    System.out.print(arr[i][j] + "\t");
                }
            }
        }
        System.out.println();
        System.out.println("Столбец с индексом " + p + ": ");
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j <arr[i].length ; j++) {
                if (j == p) {
                    System.out.println(arr[i][j] + "\t");
                }
            }
        }
    }

    public static int[][] multiArraysTask12(int size) {
        int arr[][] = new int[size][size];
        for (int i = 0; i < arr.length; i++) {
            for (int j = 0; j <arr[i].length ; j++) {
                if(i == j) {
                    arr[i][j] = i;
                } else {
                    arr[i][j] = 0;
                }
            }
        }
        return arr;
    }





}
