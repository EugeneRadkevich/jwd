package by.training.arraysort;

import static by.training.arrays.Utils.generateArrayPositive;
import static by.training.arrays.Utils.printArray;
import static by.training.arraysort.Utils.*;



public class Client {

    public static void main(String[] args) {
        //Task 1
        System.out.println("***********************Task 1***********************");
        System.out.println("Первый исходный массив: ");
        int arrFirst[] = generateArrayPositive(7, 10);
        printArray(arrFirst);
        System.out.println("Второй исходный массив: ");
        int arrSecond[] = generateArrayPositive(4, 10);
        printArray(arrSecond);
        arraySortTask1(arrFirst, arrSecond, 3);

        //Task 4
        System.out.println("***********************Task 4***********************");
        System.out.println("Исходный массив: ");
        int arr4[] = generateArrayPositive(5, 10);
        printArray(arr4);
        System.out.println("Отсортированный по возрастанию массив: ");
        bubbleSorting(arr4);
        printArray(arr4);


        //Task 5
        System.out.println("***********************Task 5***********************");
        System.out.println("Исходный массив: ");
        int arr5[] = generateArrayPositive(5, 10);
        printArray(arr5);
        System.out.println("Отсортированный по убыванию массив: ");
        printArray(selectingSort(arr5));
    }
}
