package by.training.decomposition;

public class Utils {

    public static void printArray(int arr[]) {
        for (int i = 0; i < arr.length; i++) {
            // arr[] = {3, 44, 22, 33}
            if (i == 0) {
                System.out.print("arr[] = {" + arr[i] + ", ");
            } else if (i == arr.length - 1) {
                System.out.print(arr[i] + "}\n");
            } else {
                System.out.print(arr[i] + ", ");
            }
        }
    }

    public static void printArrayDouble(double arr[]) {
        for (int i = 0; i < arr.length; i++) {
            // arr[] = {3, 44, 22, 33}
            if (i == 0) {
                System.out.print("arr[] = {" + arr[i] + ", ");
            } else if (i == arr.length - 1) {
                System.out.print(arr[i] + "}\n");
            } else {
                System.out.print(arr[i] + ", ");
            }
        }
    }

    public static int[] generateArrayPositive(int size, int bound) {
        int arr[] = new int[size]; // {0, 0, 0}
        for (int i = 0; i < size; i++) {
            arr[i] = ((int) (Math.random() * bound));
        }
        return arr;
    }

    public static int[] generateArray(int size, int bound) {
        int arr[] = new int[size]; // {0, 0, 0}
        for (int i = 0; i < size; i++) {
            boolean isPositive = (int) (Math.random() * 2) == 1;
            arr[i] = ((int) (Math.random() * bound)) * (isPositive ? 1 : -1);
        }
        return arr;
    }

    public static int findMax(int arr[]) {
        int max = arr[0];
        int index = 0;
        for (int i = 1; i < arr.length; i++) {
            if (arr[i] > max) {
                max = arr[i];
                index = i;
            }

        }
        //System.out.println("Max = " + max);
        //System.out.println("Индекс Max, i = " + index);
        return max;
    }

    public static double findMaxDouble(double arr[]) {
        double max = arr[0];
        int index = 0;
        for (int i = 1; i < arr.length; i++) {
            if (arr[i] > max) {
                max = arr[i];
                index = i;
            }

        }
        //System.out.println("Max = " + max);
        //System.out.println("Индекс Max, i = " + index);
        return max;
    }

    public static int findMin(int arr[]) {
        int min = arr[0];

        for (int i = 1; i < arr.length; i++) {
            if (arr[i] < min) {
                min = arr[i];
            }

        }
        //System.out.println("Min = " + min);
        return min;
    }

    public static void printMArray(int mArr[][]) {
        for (int i = 0; i < mArr.length; i++) {
            for (int j = 0; j < mArr[i].length; j++) {
                System.out.print(mArr[i][j] + "\t");
            }
            System.out.println();
        }
    }

    public static int[][] generateMArrayPositive(int sizeI, int sizeJ, int bound) {
        int arr[][] = new int[sizeI][sizeJ];
        for (int i = 0; i < sizeI; i++) {
            for (int j = 0; j < sizeJ; j++) {
                arr[i][j] = ((int) (Math.random() * bound));
            }
        }
        return arr;
    }

    public static int[][] generateMArray(int sizeI, int sizeJ, int bound) {
        int arr[][] = new int[sizeI][sizeJ];
        for (int i = 0; i < sizeI; i++) {
            for (int j = 0; j < sizeJ; j++) {
                boolean isPositive = (int) (Math.random() * 2) == 1;
                arr[i][j] = ((int) (Math.random() * bound))* (isPositive ? 1 : -1);
            }
        }
        return arr;
    }


}
