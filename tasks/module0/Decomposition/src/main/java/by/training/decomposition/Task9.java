package by.training.decomposition;

import static by.training.decomposition.Utils.findMax;

public class Task9 {
    public  static int [] commMul (int a, int b, int c) {
        int arrMul[] = new int [10*(a + b + c)];
        for (int i = 1; i < arrMul.length; i++) {
            if (a % i == 0 && b % i == 0 && c % i == 0 ) {
                arrMul[i] = i;
            }else {
                arrMul[i] = a - 100;
            }
        }
        return arrMul;
    }

    public  static void task9 (int a, int b, int c) {
        int arrMul[] = commMul(a, b, c);
        int maxCommMul = findMax (arrMul);
        if (maxCommMul == 1) {
            System.out.println("Числа являются взаимно простыми");
        } else {
            System.out.println("Числа не являются взаимно простыми");
        }
    }

}
