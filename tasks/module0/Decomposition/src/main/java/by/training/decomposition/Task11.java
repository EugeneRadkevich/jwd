package by.training.decomposition;

import static by.training.decomposition.Utils.generateArrayPositive;
import static by.training.decomposition.Utils.printArray;

public class Task11 {
    public static int sumThreeElements (int arr [], int k, int m) {
        int sum = 0;
        if(k < m && k < arr.length && m < arr.length) {
            for (int i = 0; i < arr.length; i++) {
                if (i >= k && i <= m ) {
                    sum = sum + arr[i];
                }
            }
        }
        return sum;
    }

    public static void task11(int k, int m){
        int arr[] = generateArrayPositive (10, 20);
        System.out.println("Исходный массив: ");
        printArray(arr);
        int sum = sumThreeElements(arr, k, m);
        System.out.println("Сумма элементов с номерами от " + k + " до " + m + " : " + sum);
    }
}
