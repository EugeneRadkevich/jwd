package by.training.decomposition;

public class Task1 {
    //Task 1


        public static double firstSide (double x1, double y1, double x2, double y2){
            double firstSide = Math.sqrt(Math.pow((y1 - y2), 2) + Math.pow((x1 - x2), 2));
            return firstSide;
        }

        public static double secondSide (double x2, double y2, double x3, double y3){
            double secondSide = Math.sqrt(Math.pow((y2 - y3), 2) + Math.pow((x2 - x3), 2));
            return secondSide;
        }

        public static double thirdSide (double x1, double y1, double x3, double y3){
            double thirdSide = Math.sqrt(Math.pow((y1 - y3), 2) + Math.pow((x1 - x3), 2));
            return thirdSide;
        }

        public static double halfPer (double firstSide, double secondSide, double thirdSide) {
            double halfPer = (firstSide + secondSide + thirdSide) / 2;
            return halfPer;
        }

        public static double area (double firstSide, double secondSide, double thirdSide, double halfPer) {
            double ar = Math.sqrt(halfPer * (halfPer - firstSide) * (halfPer - secondSide) * (halfPer - thirdSide));
            return ar;

        }

        public static void areaTriangle (double x1, double y1, double x2, double y2, double x3, double y3) {
            double sideA = firstSide(x1, y1, x2, y2);
            double sideB = secondSide(x2, y2, x3, y3);
            double sideC = thirdSide(x1, y1, x3, y3);
            double halfPer = halfPer(sideA, sideB, sideC);
            double ar = area(sideA, sideB, sideC, halfPer);
            System.out.println("Площадь треугольника: " + ar);
        }










}
