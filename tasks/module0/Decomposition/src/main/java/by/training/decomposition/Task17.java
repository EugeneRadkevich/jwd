package by.training.decomposition;

import static by.training.decomposition.Utils.printArray;

public class Task17 {

    public static int getCountsOfDigits(long number) {
        int count = (number == 0) ? 1 : 0;
        while (number != 0) {
            count++;
            number /= 10;
        }
        return count;
    }

    public static int [] digitsNumbers (int n, long number) {
        int arrDigitsNum[] = new int[n];
        long num = number;
        for (int i = 0; i < arrDigitsNum.length; i++) {
           //while (num != 0) {
                //num = (num/10);
                arrDigitsNum[i] = (int) (num / Math.pow(10, (n - (i + 1))));
                num = (long) (num - ((int)(num / Math.pow(10, (n - (i + 1)))))*Math.pow(10,(n-(i+1))));
                //break;
            //}
        }
        return arrDigitsNum;

    }

    public static boolean armstrongNumber (int arr[], int n, long number) {
        int sum = 0;
        for (int i = 0; i <arr.length ; i++) {
            sum = (int) (sum + Math.pow(arr[i], n));
        }
        if (sum == number) {
            return true;
        } else {
            return false;
        }

    }

    public static int[] arrToK (int k){
        int arr[] = new int[k];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = i+1;
        }

        return arr;
    }

    public static boolean check (long number) {
        boolean flag = false;
        int count = getCountsOfDigits(number);
        int arrDigitsNum [] = digitsNumbers(count, number);
        flag = armstrongNumber(arrDigitsNum, count, number);
        return flag;
    }


    public static void task17 (int k) {
        int arr[] = arrToK(k);
        System.out.println("В массиве :");
        printArray(arr);
        System.out.println("Пристутствуют следующие числа Амстронга :");

        for (int i = 0; i <arr.length ; i++) {
            if (check(arr[i])) {
                System.out.println("Число Армстронга " + arr[i]);
            }

        }
    }




}
