package by.training.decomposition;

import static by.training.decomposition.Task1.areaTriangle;
import static by.training.decomposition.Task10.task10;
import static by.training.decomposition.Task11.task11;
import static by.training.decomposition.Task14.getCountsOfDigits;
import static by.training.decomposition.Task14.task14;
import static by.training.decomposition.Task16.task16;
import static by.training.decomposition.Task17.digitsNumbers;
import static by.training.decomposition.Task17.task17;
import static by.training.decomposition.Task18.arrNumBitN;
import static by.training.decomposition.Task18.task18;
import static by.training.decomposition.Task2.*;
import static by.training.decomposition.Task3.task3;
import static by.training.decomposition.Task4.task4;
import static by.training.decomposition.Task5.task5;
import static by.training.decomposition.Task6.task6;
import static by.training.decomposition.Task7.*;
import static by.training.decomposition.Task8.task8;
import static by.training.decomposition.Task9.task9;
import static by.training.decomposition.Utils.*;

public class Main {

    public static void main(String[] args) {
        //Task 1
        System.out.println("***********************Task 1***********************");
        areaTriangle (1, 1, 2, 3, 4, 2);

        //Task 2
        System.out.println("***********************Task 2***********************");
        task2(12, 14);

        //Task 3
        System.out.println("***********************Task 3***********************");
        task3(36, 48, 56, 100);

        //Task 4
        System.out.println("***********************Task 4***********************");
        task4(4, 8, 6);

        //Task 5
        System.out.println("***********************Task 5***********************");
        task5(4, 8, 6);

        //Task 6
        System.out.println("***********************Task 6***********************");
        task6(4);

        //Task 7
        System.out.println("***********************Task 7***********************");
        task7(4);

        //Task 8
        System.out.println("***********************Task 8***********************");
        task8(4);

        //Task 9
        System.out.println("***********************Task 9***********************");
        task9(3, 5, 7);

        //Task 10
        System.out.println("***********************Task 10***********************");
        task10();

        //Task 11
        System.out.println("***********************Task 11***********************");
        task11(3, 5);

        //Task 14
        System.out.println("***********************Task 14***********************");
        task14(315, 3456);

        //Task 16
        System.out.println("***********************Task 16***********************");
        task16(6);

        //Task 17
        System.out.println("***********************Task 17***********************");
        task17(1000);

        //Task 18
        System.out.println("***********************Task 18***********************");
        task18(3);



    }
}
