package by.training.decomposition;

import static by.training.decomposition.Utils.findMax;
import static by.training.decomposition.Utils.findMin;

public class Task3 {

    public  static int [] commMul (int a, int b, int c, int d) {
        int arrMul[] = new int [10*(a + b + c + d)];
        for (int i = 1; i < arrMul.length; i++) {
            if (a % i == 0 && b % i == 0 && c % i == 0 && d % i ==0) {
                arrMul[i] = i;
            }else {
                arrMul[i] = a - 100;
            }
        }
        return arrMul;
    }

    public  static int maxCommMul (int arrMul[]) {
        int maxCommMul = findMax (arrMul);
        return maxCommMul;
    }

    public static void task3 (int a, int b, int c, int d){
        int arrMul[] = commMul(a, b, c, d);
        int maxCommMul = maxCommMul(arrMul);
        System.out.println("Наибольший общий делитель чисел " + a + " , " + b + " , " + c + " , " + d + " : " + maxCommMul);
    }
}
