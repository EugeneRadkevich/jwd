package by.training.decomposition;

public class Task14 {

    public static int getCountsOfDigits(long number) {
        int count = (number == 0) ? 1 : 0;
        while (number != 0) {
            count++;
            number /= 10;
        }
        return count;
    }

    public static void task14 (long numberA, long numberB) {
        int countA = getCountsOfDigits(numberA);
        int countB = getCountsOfDigits(numberB);
        if (countA > countB) {
            System.out.println("Число цифр в первом числе больше");
        } else {
            System.out.println("Число цифр во втором числе больше");
        }
    }
}
