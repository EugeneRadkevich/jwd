package by.training.decomposition;

import static by.training.decomposition.Utils.printArray;
import static by.training.decomposition.Utils.printMArray;

public class Task16 {

    public static int[] arrNto2N (int n) {
        int arr [] = new int[n+1];
        for (int i = 0; i < arr.length; i++) {
            arr[i] = i + n;
        }
        return arr;
    }

    public static int[][] arrTwins (int arr[] , int n) {
        int arrTw [][] = new int[n-1][2];
        for (int i = 0; i < arrTw.length; i++) {
            if(arr[i + 2] <= 2 * n) {
                arrTw[i][0] = arr[i];
                arrTw[i][1] = arr[i + 2];
            }
        }
        return arrTw;
    }

    public static void task16(int n) {
        int arr[] = arrNto2N(n);
        System.out.println("Исходный массив: ");
        printArray(arr);
        int arrTw[][] = arrTwins(arr, n);
        System.out.println("Пары близнецов на участке от " + n + " до " + 2 * n + " : ");
        printMArray(arrTw);
    }

}
