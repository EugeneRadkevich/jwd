package by.training.decomposition;
import static by.training.decomposition.Utils.findMax;
import static by.training.decomposition.Utils.findMin;

public class Task2 {

    public  static int [] commMul (int a, int b) {
        int arrMul[] = new int [10*(a + b)];
        for (int i = 1; i < arrMul.length; i++) {
            if (a % i == 0 && b % i == 0) {
                arrMul[i] = i;
            }else {
                arrMul[i] = a - 100;
            }
        }
        return arrMul;
    }

    public  static int [] commonFactor (int a, int b) {
        int arrFactor[] = new int [10 * (a + b)];
        for (int i = 0; i < arrFactor.length; i++) {
            if (i % a == 0 && i % b == 0 && i != 0) {
                arrFactor[i] = i;
            }else {
                arrFactor[i] = a * 100;
            }
        }
        return arrFactor;
    }

    public  static int minCommFactor (int arrFactor[]) {
        int minCommFactor = findMin (arrFactor);
        return minCommFactor;
    }

    public  static int maxCommMul (int arrMul[]) {
        int maxCommMul = findMax (arrMul);
        return maxCommMul;
    }

    public static void task2 (int a, int b){
        int arrMul[] = commMul(a, b);
        int arrFactor[] = commonFactor(a, b);
        int minCommFactor = minCommFactor(arrFactor);
        int maxCommMul = maxCommMul(arrMul);
        System.out.println("Наименьшее общее кратное " + a + " и " + b + " : " + minCommFactor);
        System.out.println("Наибольший общий делитель " + a + " и " + b + " : " + maxCommMul);
    }

}
