package by.training.decomposition;

import static by.training.decomposition.Utils.findMin;

public class Task4 {

    public  static int [] commonFactor (int a, int b, int c) {
        int arrFactor[] = new int [10 * (a + b + c)];
        for (int i = 0; i < arrFactor.length; i++) {
            if (i % a == 0 && i % b == 0 && i % c == 0 && i != 0) {
                arrFactor[i] = i;
            }else {
                arrFactor[i] = a * 100;
            }
        }
        return arrFactor;
    }

    public  static int minCommFactor (int arrFactor[]) {
        int minCommFactor = findMin (arrFactor);
        return minCommFactor;
    }

    public static void task4 (int a, int b, int c){
        int arrFactor[] = commonFactor(a, b, c);
        int minCommFactor = minCommFactor(arrFactor);
        System.out.println("Наименьшее общее кратное чисел " + a + " , " + b + " , " + c + " : " + minCommFactor);
    }

}
