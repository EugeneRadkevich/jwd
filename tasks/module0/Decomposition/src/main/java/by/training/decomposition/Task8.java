package by.training.decomposition;

import static by.training.decomposition.Utils.findMax;
import static by.training.decomposition.Utils.printArray;

public class Task8 {
    public static int[] createArr (int n) {
        int arr[] = new int[n];
            for (int i = 0; i < n; i++) {
                arr[i] = ((int) (Math.random() * 20));
            }
        return arr;
    }

    public static int[] arrWithoutMax(int arr[]) {
        int arrWithoutMax [] = new int[arr.length-1];
        int max = findMax(arr);
        int indexJ = 0;
        for (int i = 0; i < arr.length; i++) {
            if (arr[i] != max) {
                for (int j = indexJ; j < arrWithoutMax.length; j++) {
                    arrWithoutMax[j] = arr[i];
                    indexJ++;
                    break;
                }

            }

        }
        return arrWithoutMax;
    }

    public static void task8(int n){
        int arr[] = createArr(n);
        System.out.println("Заданный массив: ");
        printArray(arr);
        int arrWithoutArr[] = arrWithoutMax(arr);
        int secondMax = findMax(arrWithoutArr);
        System.out.println("Второй по величине элемент массива: " + secondMax);
    }



}

