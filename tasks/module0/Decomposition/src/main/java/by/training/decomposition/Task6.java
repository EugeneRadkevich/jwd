package by.training.decomposition;

public class Task6 {

    public static double equilateralTriangleArea (int a) {
        double area = Math.sqrt(3) * Math.pow(a, 2) / 4;
        return area;
    }

    public  static  double hexagonArea (double area) {
        double areaHexagon = 6 * area;
        return areaHexagon;
    }
    public static void task6 (int a) {
        double area = equilateralTriangleArea(a);
        double areaHexagon = hexagonArea(area);
        System.out.println("Площадь правильного шестиугольника со стороной " + a + " : " + areaHexagon);
    }

}
