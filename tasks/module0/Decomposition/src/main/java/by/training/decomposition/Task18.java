package by.training.decomposition;

import static by.training.decomposition.Utils.printArray;

public class Task18 {
    public static int getCountsOfDigits(long number) {
        int count = (number == 0) ? 1 : 0;
        while (number != 0) {
            count++;
            number /= 10;
        }
        return count;
    }

    public static int [] digitsNumbers (int n, long number) {
        int arrDigitsNum[] = new int[n];
        long num = number;
        for (int i = 0; i < arrDigitsNum.length; i++) {
            arrDigitsNum[i] = (int) (num / Math.pow(10, (n - (i + 1))));
            num = (long) (num - ((int)(num / Math.pow(10, (n - (i + 1)))))*Math.pow(10,(n-(i+1))));
        }
        return arrDigitsNum;
    }

    public static int[] arrNumBitN (int n){
        int arr[] = new int[(int) (Math.pow(10, (n)) - Math.pow(10, (n-1)))];
        for (int i = 0; i < arr.length; i++) {
            arr[0] = (int) (Math.pow(10, (n-1)));
            arr[i] = arr[0] + i;
        }
        return arr;
    }

    public static boolean checkStrictlyIncreasing (int arr[]) {
        boolean flag = true;
        for (int i = 1; i < arr.length; i++) {
            if(arr[i] <= arr[i-1]){
                flag = false;
                break;
            }
        }
        return flag;
    }

    public static boolean check (int n, long number) {
        boolean flag = false;
        //int count = getCountsOfDigits (number);
        int arr[] = digitsNumbers(n, number);
        if (checkStrictlyIncreasing(arr)) {
            flag = true;
        } else flag = false;
        return flag;
    }

    public static void task18(int n) {
        int arrNumBitN [] = arrNumBitN(n);
        System.out.println("Исходный " + n + "разрядный массив: ");
        printArray(arrNumBitN);
        for (int i = 0; i < arrNumBitN.length; i++) {
            if(check(n, arrNumBitN[i])){
                System.out.println("Число со строго возрастающими цифрами: " + arrNumBitN[i]);
            }

        }
    }

}
