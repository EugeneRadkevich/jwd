package by.training.decomposition;

import static by.training.decomposition.Utils.*;

public class Task7 {
    public static int[][] pointCoordinates(int n) {
        int arr[][] = new int[n][2];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < 2; j++) {
                arr[i][j] = ((int) (Math.random() * 20));
            }
        }
        return arr;
    }

    public static double[] distanceBetweenPoints (int arr[][]) {


        double[] arrDis = new double[arr.length-1];

        for (int i = 0; i <arr.length-1 ; i++) {
            for (int j = 0; j < 1; j++) {
                arrDis[i] = Math.sqrt(Math.pow((arr[i][j]-arr[i+1][j]), 2) + Math.pow((arr[i][j+1] - arr[i+1][j+1]), 2));
            }
        }
        return arrDis;
    }



    public static void task7 (int n) {
        int arr[][] = pointCoordinates(n);
        printMArray(arr);
        double arrDis[] = distanceBetweenPoints(arr);
        printArrayDouble(arrDis);
        double max = arrDis[0];
        int index = 0;
        for (int i = 1; i < arrDis.length; i++) {
            if (arrDis[i] > max) {
                max = arrDis[i];
                index = i;
            }
        }
        System.out.println("Максимальное расстояние " + max + " между точками " + index + " и " + (index + 1));



    }
}
