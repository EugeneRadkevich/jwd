package by.training.decomposition;

import static by.training.decomposition.Utils.printArray;

public class Task10 {

    public static int calculateFactorial (int n) {
        int res = 1;
        for (int i = 1; i <= n; i++) {
            res = res * i;
        }
        return res;
    }
    public static int[] arrOdd () {
        int arrOdd[] = new int[5];
        int indexJ = 0;
        for (int i = 0; i <= 9; i++) {
            if(i % 2 != 0) {
                for (int j = indexJ; j < arrOdd.length; j++) {
                    arrOdd[j] = i;
                    indexJ++;
                    break;
                }
            }
        }
        return arrOdd;
    }

    public static void  task10 (){
        int arrOdd[] = arrOdd();
        int sum = 0;
        System.out.println("Массив из нечетных чисел от 1 до 9: ");
        printArray(arrOdd);
        for (int i = 0; i < arrOdd.length; i++) {
            int factorial = calculateFactorial(arrOdd[i]);
            sum = sum + factorial;
        }
        System.out.println("Сумма факториалов нечетных чисел от 1 до 9: " + sum);
    }
}
