package by.training.decomposition;

import static by.training.decomposition.Utils.findMax;
import static by.training.decomposition.Utils.findMin;

public class Task5 {

    public static int[] newArray(int a, int b, int c) {
        int arr[] = {a, b, c};
        return arr;
    }

    public static void task5 (int a, int b, int c) {
        int arr[] = newArray(a, b, c);
        int max = findMax(arr);
        int min = findMin(arr);
        System.out.println("Сумма наибольшего и наименьшего чисел " + a + " , " + b + " , " + c + " : " + (min + max));
    }
}
