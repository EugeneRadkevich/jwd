package by.training.oop_basics.task1;

abstract class File {

    String name;
    Directory dir;

    public boolean createFile (Directory dir, String name) {
        System.out.println("File created");
        return true;
    }

    public abstract boolean createFile(Directory dir);

    public boolean deleteFile (File file) {
        System.out.println("File deleted");
        return true;
    }

    public boolean renameFile (File file) {
        System.out.println("File renamed");
        return true;
    }

    public boolean updateFile (File file) {
        System.out.println("File updated");
        return true;
    }

    public void outPut (File file) {
        System.out.println("Output file");
    }

}
