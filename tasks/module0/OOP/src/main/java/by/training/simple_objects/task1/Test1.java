package by.training.simple_objects.task1;

public class Test1 {
    int a;
    int b;

    public boolean updateA (int aNew) {
        a = aNew;
        return true;
    }

    public boolean updateB (int bNew) {
        b = bNew;
        return true;
    }

    public void max () {
        if (a > b) {
            System.out.println("Наибольшее значение: " + a);
        } else {
            System.out.println("Наибольшее значение: " + b);
        }
    }

    public void sum() {
        System.out.println("Сумма двух значений: " + (a + b));
    }

    @Override
    public String toString() {
        return "Test1{" +
                "a=" + a +
                ", b=" + b +
                '}';
    }
}
