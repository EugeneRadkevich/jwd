package by.training.aggregation_composition.task1;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        Word word1 = new Word("Есть ");
        Word word2 = new Word("ли ");
        Word word3 = new Word("оправдание ");
        Word word4 = new Word("Сальери, ");
        Word word5 = new Word("который ");
        Word word6 = new Word("отравил ");
        Word word7 = new Word("Моцарта? ");
        List<Word> contentSent1 = new ArrayList<>();
        contentSent1.add(word1);
        contentSent1.add(word2);
        contentSent1.add(word3);
        contentSent1.add(word4);
        contentSent1.add(word5);
        contentSent1.add(word6);
        contentSent1.add(word7);
        Sentence sentence1 = new Sentence(contentSent1);
        Word word8 = new Word("Все ");
        Word word9 = new Word("говорят: ");
        Word word10 = new Word("нет ");
        Word word11 = new Word("правды ");
        Word word12 = new Word("на ");
        Word word13 = new Word("земле. ");
        List<Word> contentSent2 = new ArrayList<>();
        contentSent2.add(word8);
        contentSent2.add(word9);
        contentSent2.add(word10);
        contentSent2.add(word11);
        contentSent2.add(word12);
        contentSent2.add(word13);
        Sentence sentence2 = new Sentence(contentSent2);
        List<Sentence> contentText = new ArrayList<>();
        contentText.add(sentence1);
        contentText.add(sentence2);
        Text text = new Text("Моцарт и Сальери", contentText);
        System.out.println(text.heading);
        text.printText(text);
        Word word14 = new Word("На ");
        Word word15 = new Word("этот ");
        Word word16 = new Word("же ");
        Word word17 = new Word("путь ");
        Word word18 = new Word("встанет ");
        Word word19 = new Word("и ");
        Word word20 = new Word("Родион ");
        Word word21 = new Word("Раскольников. ");

        List<Word> contentSent3 = new ArrayList<>();
        contentSent3.add(word14);
        contentSent3.add(word15);
        contentSent3.add(word16);
        contentSent3.add(word17);
        contentSent3.add(word18);
        contentSent3.add(word19);
        contentSent3.add(word20);
        contentSent3.add(word21);
        Sentence sentence3 = new Sentence(contentSent3);
        text.addSentence(sentence3);
        text.printText(text);





    }
}
