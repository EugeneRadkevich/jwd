package by.training.simple_objects.task7;

import by.training.simple_objects.task7.model.Point;
import by.training.simple_objects.task7.model.Side;
import by.training.simple_objects.task7.model.Triangle;
import by.training.simple_objects.task7.service.TriangleServiceImpl;

public class Main {
    public static void main(String[] args) {


        Point a = new Point(1, 1);
        Point b = new Point(2, 3);
        Point c = new Point(3, 2);
        Side sideA = new Side(a, b);
        Side sideB = new Side(b, c);
        Side sideC = new Side(c, a);
        Triangle triangle = new Triangle(sideA, sideB, sideC);
        TriangleServiceImpl tService = new TriangleServiceImpl();
        System.out.print("Площадь треугольника: ");
        System.out.println(tService.area(triangle));
        System.out.print("Периметр треугольника: ");
        System.out.println(tService.perimeter(triangle));
        System.out.print("Точка пересечения медиан: ");
        System.out.println(tService.meridianIntersection(triangle));
    }
}
