package by.training.simple_objects.task4.main;

import by.training.simple_objects.task4.model.Train;
import by.training.simple_objects.task4.service.TrainServiceImpl;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        Train train2 = new Train("Minsk", 2, "19:45");
        Train train5 = new Train("Brest", 5, "18:45");
        Train train4 = new Train("Gomel", 4, "17:45");
        Train train6 = new Train("Mogilev", 6, "16:45");
        Train train9 = new Train("Minsk", 9, "14:45");

        List<Train> trains = new ArrayList<>();
        trains.add(train2);
        trains.add(train5);
        trains.add(train4);
        trains.add(train6);
        trains.add(train9);

        TrainServiceImpl trainService = new TrainServiceImpl();
        System.out.println(trainService.getTrainByNumber(trains, 10));
        System.out.println(trainService.sortTrainByNumber(trains));
        System.out.println(trainService.sortTrainByDestination(trains));

    }

}
