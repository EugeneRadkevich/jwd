package by.training.simple_objects.task7.model;

import java.util.List;

public class Triangle {

    Side sideA;
    Side sideB;
    Side sideC;

    public Triangle(Side sideA, Side sideB, Side sideC) {
        this.sideA = sideA;
        this.sideB = sideB;
        this.sideC = sideC;
    }

    public Side getSideA() {
        return sideA;
    }

    public void setSideA(Side sideA) {
        this.sideA = sideA;
    }

    public Side getSideB() {
        return sideB;
    }

    public void setSideB(Side sideB) {
        this.sideB = sideB;
    }

    public Side getSideC() {
        return sideC;
    }

    public void setSideC(Side sideC) {
        this.sideC = sideC;
    }


}
