package by.training.simple_objects.task7.service;

import by.training.simple_objects.task7.model.Side;
import by.training.simple_objects.task7.model.Triangle;

import java.util.ArrayList;
import java.util.List;

public class SideCalculateImpl implements SideCalculate {


    @Override
    public double lengthSide(Side side) {
        double length = (Math.sqrt(Math.pow((side.getSecond().getY() - side.getFirst().getY()), 2) +
                Math.pow((side.getSecond().getX() - side.getFirst().getX()), 2)));
        return length;
    }
}
