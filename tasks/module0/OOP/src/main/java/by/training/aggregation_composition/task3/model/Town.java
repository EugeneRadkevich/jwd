package by.training.aggregation_composition.task3.model;

public class Town {
    String nameTown;
    boolean isCapital;
    boolean isRegionalCenter;

    public Town(String nameTown, boolean isCapital, boolean isRegionalCenter) {
        this.nameTown = nameTown;
        this.isCapital = isCapital;
        this.isRegionalCenter = isRegionalCenter;
    }

    public String getNameTown() {
        return nameTown;
    }

    public void setNameTown(String nameTown) {
        this.nameTown = nameTown;
    }

    public boolean isCapital() {
        return isCapital;
    }

    public void setCapital(boolean capital) {
        isCapital = capital;
    }

    public boolean isRegionalCenter() {
        return isRegionalCenter;
    }

    public void setRegionalCenter(boolean regionalCenter) {
        isRegionalCenter = regionalCenter;
    }


    @Override
    public String toString() {
        return nameTown;
    }
}
