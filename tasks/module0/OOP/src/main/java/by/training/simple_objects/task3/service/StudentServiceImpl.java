package by.training.simple_objects.task3.service;

import by.training.simple_objects.task3.model.Student;

import java.util.ArrayList;
import java.util.List;

public class StudentServiceImpl implements StudentService {
    @Override
    public void selectStudentProgressUp9(List<Student> students) {
        List<Student> goodStudents = new ArrayList<>();
        for (int i = 0; i <students.size() ; i++) {
            for (int j = 0; j <students.get(i).getProgress().size() ; j++) {
                if (students.get(i).getProgress().get(j) < 9) {
                    break;
                } else {
                    if (!goodStudents.contains(students.get(i))) {
                        goodStudents.add(students.get(i));
                    }
                }
            }
        }
        System.out.println("Студенты с отметками 9 и выше: ");
        for (int i = 0; i <goodStudents.size() ; i++) {
            System.out.println("ФИО студента: " + goodStudents.get(i).getName() + ", номер группы: "
                    + goodStudents.get(i).getNumbGroup());


        }
    }
}
