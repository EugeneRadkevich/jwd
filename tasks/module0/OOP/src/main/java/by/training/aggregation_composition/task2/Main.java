package by.training.aggregation_composition.task2;

public class Main {
    public static void main(String[] args) {
        Car car = new Car();
        car.model = "BMW";
        car.engine = new Engine(300);
        car.wheels = new Wheel(true);
        car.go();
        car.refuel();
        car.wheels.change();
    }
}
