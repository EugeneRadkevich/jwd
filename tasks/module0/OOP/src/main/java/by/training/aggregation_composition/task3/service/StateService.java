package by.training.aggregation_composition.task3.service;

import by.training.aggregation_composition.task3.model.State;
import by.training.aggregation_composition.task3.model.Town;

import java.util.List;

public interface StateService {
    Town capitalState(State state);
    int numberRegion(State state);
    double areaState(State state);
    List<Town> townsRegionsState(State state);
}
