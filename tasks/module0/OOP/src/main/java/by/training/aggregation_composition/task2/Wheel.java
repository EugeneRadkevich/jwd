package by.training.aggregation_composition.task2;

public class Wheel {
    boolean hasWheels;

    public Wheel(boolean hasWheels) {
        this.hasWheels = hasWheels;
    }

    public void change() {
        System.out.println("Меняем колесо!");
    }
}
