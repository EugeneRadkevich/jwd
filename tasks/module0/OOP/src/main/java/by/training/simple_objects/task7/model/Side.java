package by.training.simple_objects.task7.model;

import java.util.List;

public class Side {
    Point first;
    Point second;

    public Side(Point first, Point second) {
        this.first = first;
        this.second = second;
    }


    /*public double getLength() {
        // Очень сорри(((((((
        double length = (Math.sqrt(Math.pow((second.getY() - first.getY()), 2) +
                Math.pow((second.getX() - first.getX()), 2)));
        return length;
    }*/

    public Point getFirst() {
        return first;
    }

    public void setFirst(Point first) {
        this.first = first;
    }

    public Point getSecond() {
        return second;
    }

    public void setSecond(Point second) {
        this.second = second;
    }
}


