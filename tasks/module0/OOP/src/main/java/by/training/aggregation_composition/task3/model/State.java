package by.training.aggregation_composition.task3.model;

import java.util.List;

public class State {
    String nameState;
    List <Region> regions;

    public State(String nameState, List<Region> regions) {
        this.nameState = nameState;
        this.regions = regions;
    }

    public String getNameState() {
        return nameState;
    }

    public void setNameState(String nameState) {
        this.nameState = nameState;
    }

    public List<Region> getRegions() {
        return regions;
    }

    public void setRegions(List<Region> regions) {
        this.regions = regions;
    }

    @Override
    public String toString() {
        return "State{" +
                "nameState='" + nameState + '\'' +
                ", regions=" + regions +
                '}';
    }
}
