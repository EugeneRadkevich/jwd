package by.training.aggregation_composition.task3.service;

import by.training.aggregation_composition.task3.model.District;

public interface DistrictService {
    double districtArea(District district);

}
