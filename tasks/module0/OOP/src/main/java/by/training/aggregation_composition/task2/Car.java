package by.training.aggregation_composition.task2;

public class Car {

    String model;
    Engine engine;
    Wheel wheels;

    public Car(String model, Engine engine, Wheel wheels) {
        this.model = model;
        this.engine = engine;
        this.wheels = wheels;
    }

    public Car() {
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public Engine getEngine() {
        return engine;
    }

    public void setEngine(Engine engine) {
        this.engine = engine;
    }

    public Wheel getWheels() {
        return wheels;
    }

    public void setWheels(Wheel wheels) {
        this.wheels = wheels;
    }

    public void go(){
        System.out.println("Поехали!");
    }

    public void refuel() {
        System.out.println("Заправляемся!");
    }
}
