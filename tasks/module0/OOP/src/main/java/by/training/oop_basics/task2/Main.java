package by.training.oop_basics.task2;

import java.util.ArrayList;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        Product phone = new Product("apple", 1000);
        Product note = new Product("LG", 1500);
        Product watch = new Product("Xiaomi", 500);
        List<Product> products= new ArrayList<>();
        products.add(phone);
        products.add(note);
        products.add(watch);
        Payment payment = new Payment();
        Payment.Transaction transaction = new Payment.Transaction(products);
        payment.start();
        transaction.execution(products);

    }
}
