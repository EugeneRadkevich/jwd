package by.training.simple_objects.task4.service;

import by.training.simple_objects.task4.model.Train;

import java.util.List;

public interface TrainService {
    List<Train> sortTrainByNumber(List<Train> trains);
    Train getTrainByNumber(List<Train> trains, int number);
    List<Train> sortTrainByDestination(List<Train> trains);
}
