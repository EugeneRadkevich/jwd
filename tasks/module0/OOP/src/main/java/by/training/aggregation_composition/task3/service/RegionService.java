package by.training.aggregation_composition.task3.service;

import by.training.aggregation_composition.task3.model.Region;
import by.training.aggregation_composition.task3.model.Town;

public interface RegionService {
    double regionArea(Region region);
    Town townRegion(Region region);


}
