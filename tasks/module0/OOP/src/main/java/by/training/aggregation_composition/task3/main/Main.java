package by.training.aggregation_composition.task3.main;

import by.training.aggregation_composition.task3.model.District;
import by.training.aggregation_composition.task3.model.Region;
import by.training.aggregation_composition.task3.model.State;
import by.training.aggregation_composition.task3.model.Town;
import by.training.aggregation_composition.task3.service.StateService;
import by.training.aggregation_composition.task3.service.StateServiceImpl;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        Town townMinsk = new Town("Минск", true, true);
        Town townMachulischi = new Town("Мачулищи", false, false);
        Town townBrest = new Town("Брест", false, true);
        Town townBaranovichi = new Town("Барановичи", false, false);
        Town townGorodische = new Town("Городище", false, false);
        Town townVitebsk = new Town("Витебск", false, true);
        Town townPolock = new Town("Полоцк", false, false);
        Town townFarinovo = new Town("Фариново", false, false);
        Town townGrodno = new Town("Гродно", false, true);
        Town townMosti = new Town("Мосты", false, false);
        Town townPeski = new Town("Пески", false, false);
        Town townGomel = new Town("Гомель", false, true);
        Town townRechica = new Town("Речица", false, false);
        Town townGoroshkov = new Town("Горошков", false, false);
        //Town townMogilev = new Town("Могилев", false, true);
        Town townChausi = new Town("Чаусы", false, false);
        Town townRosinka = new Town("Росинка", false, false);
        Town townDerzinsk = new Town("Дзержинск", false, false);
        Town townNegoreloe = new Town("Негорелое", false, false);

        List <Town> minskTowns = new ArrayList<>();
        minskTowns.add(townMinsk);
        minskTowns.add(townMachulischi);
        List <Town> derzinskTowns = new ArrayList<>();
        derzinskTowns.add(townDerzinsk);
        derzinskTowns.add(townNegoreloe);
        List <Town> brestTowns = new ArrayList<>();
        brestTowns.add(townBrest);
        List <Town> baranovichiTowns = new ArrayList<>();
        baranovichiTowns.add(townBaranovichi);
        baranovichiTowns.add(townGorodische);
        List <Town> vitebskTowns = new ArrayList<>();
        vitebskTowns.add(townVitebsk);
        List <Town> polockTowns = new ArrayList<>();
        polockTowns.add(townPolock);
        polockTowns.add(townFarinovo);
        List <Town> grodnoTowns = new ArrayList<>();
        grodnoTowns.add(townGrodno);
        //List <Town> mogilevTowns = new ArrayList<>();
        //minskTowns.add(townMogilev);
        List <Town> chausiTowns = new ArrayList<>();
        chausiTowns.add(townChausi);
        chausiTowns.add(townRosinka);
        List <Town> gomelTowns = new ArrayList<>();
        gomelTowns.add(townGomel);
        List <Town> mostiTowns = new ArrayList<>();
        mostiTowns.add(townMosti);
        mostiTowns.add(townPeski);
        List <Town> rechicaTowns = new ArrayList<>();
        rechicaTowns.add(townRechica);
        rechicaTowns.add(townGoroshkov);


        District districtMinsk = new District("Барановический",232, minskTowns);
        District districtDerzinsk =new District("Дзержинский", 123, derzinskTowns);
        District districtBrest =new District("Бресткий", 223, brestTowns);
        District districtBaranovichi =new District("Барановический", 333, baranovichiTowns);
        District districtVitebsk =new District("Витебский", 444, vitebskTowns);
        District districtPolock =new District("Полоцкий", 124, polockTowns);
        District districtGrodno =new District("Гродненский", 125, grodnoTowns);

        List <District> minskDistricts = new ArrayList<>();
        minskDistricts.add(districtMinsk);
        minskDistricts.add(districtDerzinsk);

        List <District> brestDistricts = new ArrayList<>();
        brestDistricts.add(districtBaranovichi);
        brestDistricts.add(districtBrest);

        List <District> vitebskDistricts = new ArrayList<>();
        vitebskDistricts.add(districtVitebsk);
        vitebskDistricts.add(districtPolock);

        List <District> grodnoDistricts = new ArrayList<>();
        grodnoDistricts.add(districtGrodno);


        Region regionMinsk = new Region(minskDistricts, "Минская");
        Region regionBrest = new Region(brestDistricts, "Брестский");
        Region regionVitebsk = new Region(vitebskDistricts, "Витебский");
        Region regionGrodno = new Region(grodnoDistricts, "Гродненский");


        List <Region> belarusRegions = new ArrayList<>();
        belarusRegions.add(regionBrest);
        belarusRegions.add(regionGrodno);
        belarusRegions.add(regionMinsk);
        belarusRegions.add(regionVitebsk);


        State belarus = new State("Беларусь", belarusRegions);
        StateServiceImpl belarusImpl = new StateServiceImpl();
        System.out.print("Столица: ");
        System.out.println(belarusImpl.capitalState(belarus));
        System.out.print("Количестов областей: ");
        System.out.println(belarusImpl.numberRegion(belarus));
        System.out.print("Площадь: ");
        System.out.println(belarusImpl.areaState(belarus));
        System.out.print("Областные центры: ");
        System.out.println(belarusImpl.townsRegionsState(belarus));




    }

}
