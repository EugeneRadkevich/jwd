package by.training.aggregation_composition.task3.model;

import java.util.List;

public class District {
    String nameDistrict;
    double areaDistrict;
    List<Town> townsDistrict;

    public District(String nameDistrict, double areaDistrict, List<Town> townsDistrict) {
        this.nameDistrict = nameDistrict;
        this.areaDistrict = areaDistrict;
        this.townsDistrict = townsDistrict;
    }

    public String getNameDistrict() {
        return nameDistrict;
    }

    public void setNameDistrict(String nameDistrict) {
        this.nameDistrict = nameDistrict;
    }

    public double getAreaDistrict() {
        return areaDistrict;
    }

    public void setAreaDistrict(double areaDistrict) {
        this.areaDistrict = areaDistrict;
    }

    public List<Town> getTownsDistrict() {
        return townsDistrict;
    }

    public void setTownsDistrict(List<Town> townsDistrict) {
        this.townsDistrict = townsDistrict;
    }

    @Override
    public String toString() {
        return "District{" +
                "nameDistrict='" + nameDistrict + '\'' +
                ", areaDistrict=" + areaDistrict +
                ", townsDistrict=" + townsDistrict +
                '}';
    }
}
