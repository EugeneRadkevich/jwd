package by.training.aggregation_composition.task3.service;

import by.training.aggregation_composition.task3.model.Town;

public class TownServiceImpl implements TownService {
    @Override
    public boolean isCapital(Town town) {
        if (town.isCapital()){
            return true;
        }
        else {
            return false;
        }

    }

    @Override
    public boolean isRegionTown(Town town) {
        if (town.isRegionalCenter()) {
            return true;
        }
        else {
            return false;
        }

    }
}
