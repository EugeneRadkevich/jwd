package by.training.aggregation_composition.task3.service;

import by.training.aggregation_composition.task3.model.District;
import by.training.aggregation_composition.task3.model.Region;
import by.training.aggregation_composition.task3.model.Town;

import java.util.List;

public class RegionServiceImpl implements RegionService {
    @Override
    public double regionArea(Region region) {
        double area = 0;
        for (int i = 0; i <region.getDistricts().size() ; i++) {
            area = area + region.getDistricts().get(i).getAreaDistrict();
        }
        return area;
    }

    @Override
    public Town townRegion(Region region) {
        Town town = null;
        for (int i = 0; i <region.getDistricts().size() ; i++) {
            //region.getDistricts() =
            for (int j = 0; j <region.getDistricts().get(i).getTownsDistrict().size(); j++) {
                if (region.getDistricts().get(i).getTownsDistrict().get(j).isRegionalCenter()){
                    town =  region.getDistricts().get(i).getTownsDistrict().get(j);
                }
            }
        }
        return town;
    }
}
