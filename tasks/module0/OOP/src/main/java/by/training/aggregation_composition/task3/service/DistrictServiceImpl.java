package by.training.aggregation_composition.task3.service;

import by.training.aggregation_composition.task3.model.District;

public class DistrictServiceImpl implements DistrictService {
    @Override
    public double districtArea(District district) {
        double area = district.getAreaDistrict();
        return area;
    }
}
