package by.training.simple_objects.task3.model;

import java.util.List;

public class Student {
    String name;
    int numbGroup;
    List<Integer> progress;

    public Student(String name, int numbGroup, List<Integer> progress) {
        this.name = name;
        this.numbGroup = numbGroup;
        this.progress = progress;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNumbGroup() {
        return numbGroup;
    }

    public void setNumbGroup(int numbGroup) {
        this.numbGroup = numbGroup;
    }

    public List<Integer> getProgress() {
        return progress;
    }

    public void setProgress(List<Integer> progress) {
        this.progress = progress;
    }

    @Override
    public String toString() {
        return "Student{" +
                "name='" + name + '\'' +
                ", numbGroup=" + numbGroup +
                ", progress=" + progress +
                '}';
    }
}
