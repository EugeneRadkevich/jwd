package by.training.simple_objects.task3.main;

import by.training.simple_objects.task3.model.Student;
import by.training.simple_objects.task3.service.StudentServiceImpl;

import javax.rmi.CORBA.Stub;
import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        List<Integer> progressIvanov = new ArrayList<>();
        progressIvanov.add(5);
        progressIvanov.add(8);
        progressIvanov.add(9);
        progressIvanov.add(7);
        progressIvanov.add(10);

        List<Integer> progressPetrov = new ArrayList<>();
        progressPetrov.add(7);
        progressPetrov.add(9);
        progressPetrov.add(9);
        progressPetrov.add(10);
        progressPetrov.add(10);

        List<Integer> progressSidorov = new ArrayList<>();
        progressSidorov.add(9);
        progressSidorov.add(9);
        progressSidorov.add(9);
        progressSidorov.add(10);
        progressSidorov.add(10);

        List<Integer> progressPushkin = new ArrayList<>();
        progressPushkin.add(5);
        progressPushkin.add(6);
        progressPushkin.add(4);
        progressPushkin.add(7);
        progressPushkin.add(8);

        List<Integer> progressGricuk = new ArrayList<>();
        progressGricuk.add(7);
        progressGricuk.add(8);
        progressGricuk.add(9);
        progressGricuk.add(7);
        progressGricuk.add(10);

        List<Integer> progressMishin = new ArrayList<>();
        progressMishin.add(9);
        progressMishin.add(10);
        progressMishin.add(9);
        progressMishin.add(9);
        progressMishin.add(10);

        List<Integer> progressKlopov = new ArrayList<>();
        progressKlopov.add(7);
        progressKlopov.add(8);
        progressKlopov.add(6);
        progressKlopov.add(7);
        progressKlopov.add(4);

        List<Integer> progressMahov = new ArrayList<>();
        progressMahov.add(10);
        progressMahov.add(9);
        progressMahov.add(9);
        progressMahov.add(9);
        progressMahov.add(10);

        List<Integer> progressPehov = new ArrayList<>();
        progressPehov.add(5);
        progressPehov.add(8);
        progressPehov.add(8);
        progressPehov.add(7);
        progressPehov.add(10);

        List<Integer> progressIliev = new ArrayList<>();
        progressIliev.add(7);
        progressIliev.add(8);
        progressIliev.add(9);
        progressIliev.add(8);
        progressIliev.add(10);

        Student studentIvanov = new Student ("Иванов И.И.", 1, progressIvanov);
        Student studentPetrov = new Student ("Петров П.П.", 2, progressPetrov);
        Student studentSidorov = new Student ("Сидоров С.С.", 3, progressSidorov);
        Student studentPushkin = new Student ("Пушкин И.И.", 1, progressPushkin);
        Student studentGricuk = new Student ("Грицук К.И.", 2, progressGricuk);
        Student studentMishin = new Student ("Мишин Г.П.", 3, progressMishin);
        Student studentKlopov = new Student ("Клопов В.И.", 1, progressKlopov);
        Student studentMahov = new Student ("Махов Ю.И.", 2, progressMahov);
        Student studentPehov = new Student ("Пехов Е.И.", 3, progressPehov);
        Student studentIliev = new Student ("Ильев В.В.", 1, progressIliev);

        List<Student> students = new ArrayList<>();
        students.add(studentIliev);
        students.add(studentPetrov);
        students.add(studentSidorov);
        students.add(studentPushkin);
        students.add(studentIvanov);
        students.add(studentGricuk);
        students.add(studentMishin);
        students.add(studentKlopov);
        students.add(studentMahov);
        students.add(studentPehov);
        StudentServiceImpl excellentStudents = new StudentServiceImpl();
        excellentStudents.selectStudentProgressUp9(students);

    }
}
