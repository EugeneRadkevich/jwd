package by.training.simple_objects.task7.service;

import by.training.simple_objects.task7.model.Point;
import by.training.simple_objects.task7.model.*;

import java.util.ArrayList;
import java.util.List;

public class TriangleServiceImpl implements TriangleService {

    @Override
    public double area(Triangle triangle) {
        PerimeterCalculate perimeterCalculate = new PerimeterCalculate();
        SideCalculate sideACalculate = new SideCalculate();
        SideCalculate sideBCalculate = new SideCalculate();
        SideCalculate sideCCalculate = new SideCalculate();
        double sideALength = sideACalculate.sideCalculate(triangle.getSideA());
        double sideBLength = sideBCalculate.sideCalculate(triangle.getSideB());
        double sideCLength = sideCCalculate.sideCalculate(triangle.getSideC());
        double per = perimeterCalculate.perimeterCalculate(triangle);
        double halfPer = per/2;
        double area = Math.sqrt(halfPer * (halfPer - sideALength) * (halfPer- sideBLength) * (halfPer - sideCLength));
        return area;

    }

    @Override
    public double perimeter(Triangle triangle) {
        PerimeterCalculate perimeterCalculate = new PerimeterCalculate();
        double per = perimeterCalculate.perimeterCalculate(triangle);
        return per;
    }

    @Override
    public Point meridianIntersection(Triangle triangle) {
        double xMer = (triangle.getSideA().getFirst().getX() + triangle.getSideB().getFirst().getX() + triangle.getSideC().getFirst().getX())/3;
        double yMer = (triangle.getSideA().getFirst().getY() + triangle.getSideB().getFirst().getY() + triangle.getSideC().getFirst().getY())/3;
        Point meridianIntersection = new Point();
        meridianIntersection.setX(xMer);
        meridianIntersection.setY(yMer);
        return meridianIntersection;

    }

    public class SideCalculate {

        public double sideCalculate(Side side) {
            double length = (Math.sqrt(Math.pow((side.getSecond().getY() - side.getFirst().getY()), 2) +
                    Math.pow((side.getSecond().getX() - side.getFirst().getX()), 2)));
            return length;
        }
    }

    public class PerimeterCalculate {

        public double perimeterCalculate(Triangle triangle) {
            SideCalculate sideACalculate = new SideCalculate();
            SideCalculate sidBCalculate = new SideCalculate();
            SideCalculate sideCCalculate = new SideCalculate();
            double sideALength = sideACalculate.sideCalculate(triangle.getSideA());
            double sideBLength = sidBCalculate.sideCalculate(triangle.getSideB());
            double sideCLength = sideCCalculate.sideCalculate(triangle.getSideC());
            double perimeter = sideALength + sideBLength + sideCLength;
            return perimeter;
        }






    }


}
