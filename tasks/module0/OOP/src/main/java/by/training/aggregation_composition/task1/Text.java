package by.training.aggregation_composition.task1;

import java.util.ArrayList;
import java.util.List;

public class Text {

    String heading;
    List<Sentence> sentences;

    public Text(String heading, List<Sentence> sentences) {
        this.heading = heading;
        this.sentences = sentences;
    }

    public String getHeading() {
        return heading;
    }

    public void setHeading(String heading) {
        this.heading = heading;
    }

    public List<Sentence> getSentences() {
        return sentences;
    }

    public void setSentences(List<Sentence> sentences) {
        this.sentences = sentences;
    }

    public String printHeading (Text text) {
        return "Heading : " + heading;

    }

    public void printText (Text text){
        for (int i = 0; i <text.sentences.size() ; i++) {
            System.out.println(text.sentences.get(i));
        }
    }

    public boolean addSentence (Sentence sentenceAdd) {
        sentences.add(sentenceAdd);
        this.sentences = sentences;
        return true;
    }
}
