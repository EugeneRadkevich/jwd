package by.training.simple_objects.task2;

public class Main {

    public static void main(String[] args) {
        Test2 test2 = new Test2(3, 5);
        System.out.println(test2.toString());
        Test2 test21 = new Test2();
        test21.a = 4;
        test21.b = 8;
        System.out.println(test21.toString());
    }
}
