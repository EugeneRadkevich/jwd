package by.training.aggregation_composition.task3.service;

import by.training.aggregation_composition.task3.model.Town;

public interface TownService {

    boolean isCapital(Town town);
    boolean isRegionTown (Town town);
}
