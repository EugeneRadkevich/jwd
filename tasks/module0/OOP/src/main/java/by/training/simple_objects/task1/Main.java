package by.training.simple_objects.task1;

public class Main {
    public static void main(String[] args) {
        Test1 test1 = new Test1();
        test1.a = 2;
        test1.b = 4;
        System.out.println(test1.toString());
        test1.updateA(3);
        test1.updateB(5);
        test1.sum();
        test1.max();
        System.out.println(test1.toString());
    }
}
