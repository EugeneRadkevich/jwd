package by.training.simple_objects.task4.service;

import by.training.simple_objects.task4.model.Train;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class TrainServiceImpl implements TrainService {
    @Override
    public List<Train> sortTrainByNumber(List<Train> trains) {

        Comparator<Train> compareByNumber = new Comparator<Train>() {
            @Override
            public int compare(Train o1, Train o2) {
                return o1.getNumberTrain().compareTo(o2.getNumberTrain());
            }
        };
        Collections.sort(trains, compareByNumber);
        return trains;






    }

    @Override
    public Train getTrainByNumber(List<Train> trains, int number) {
        for (Train train : trains) {
            if(train.getNumberTrain() == number) {
                return train;
           // } else {
             //
               // return null;
            }
        }
        System.out.println("Такого поезда не существует");
        return null;
    }

    @Override
    public List<Train> sortTrainByDestination(List<Train> trains) {
        Comparator<Train> compareByDestination = new Comparator<Train>() {
            @Override
            public int compare(Train o1, Train o2) {
                int sComp = o1.getDestination().compareTo(o2.getDestination());
                if (sComp !=0){
                    return sComp;
                }
                return o1.getDepartureTime().compareTo(o2.getDepartureTime());

            }
        };
        Collections.sort(trains, compareByDestination);


        return trains;
    }
}
