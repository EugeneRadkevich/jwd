package by.training.simple_objects.task4.model;

public class Train implements Comparable<Train>{

    String destination;
    Integer numberTrain;
    String departureTime;

    public Train(String destination, int numberTrain, String departureTime) {
        this.destination = destination;
        this.numberTrain = numberTrain;
        this.departureTime = departureTime;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public Integer getNumberTrain() {
        return numberTrain;
    }

    public void setNumberTrain(Integer numberTrain) {
        this.numberTrain = numberTrain;
    }

    public String getDepartureTime() {
        return departureTime;
    }

    public void setDepartureTime(String departureTime) {
        this.departureTime = departureTime;
    }



    @Override
    public String toString() {
        return "Train{" +
                "destination='" + destination + '\'' +
                ", numberTrain=" + numberTrain +
                ", departureTime='" + departureTime + '\'' +
                '}';
    }


    @Override
    public int compareTo(Train o) {
        return this.getNumberTrain().compareTo(o.getNumberTrain());
    }
}
