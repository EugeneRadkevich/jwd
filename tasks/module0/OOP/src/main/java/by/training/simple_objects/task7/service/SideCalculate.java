package by.training.simple_objects.task7.service;

import by.training.simple_objects.task7.model.Side;
import by.training.simple_objects.task7.model.Triangle;

import java.util.List;

public interface SideCalculate {
    public double lengthSide(Side side);
}

