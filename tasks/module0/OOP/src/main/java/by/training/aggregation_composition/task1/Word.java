package by.training.aggregation_composition.task1;

public class Word {
    String contentWord;

    public Word(String contentWord) {
        this.contentWord = contentWord;
    }

    public String getContentWord() {
        return contentWord;
    }

    public void setContentWord(String contentWord) {
        this.contentWord = contentWord;
    }

    @Override
    public String toString() {
        return contentWord;
    }
}
