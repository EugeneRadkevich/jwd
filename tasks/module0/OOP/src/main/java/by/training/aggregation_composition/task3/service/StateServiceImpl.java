package by.training.aggregation_composition.task3.service;

import by.training.aggregation_composition.task3.model.State;
import by.training.aggregation_composition.task3.model.Town;

import java.util.ArrayList;
import java.util.List;

public class StateServiceImpl implements StateService {
    @Override
    public Town capitalState(State state) {
        Town capital = null;
        for (int i = 0; i <state.getRegions().size() ; i++) {
            for (int j = 0; j <state.getRegions().get(i).getDistricts().size() ; j++) {
                for (int k = 0; k <state.getRegions().get(i).getDistricts().get(j).getTownsDistrict().size() ; k++) {
                    if (state.getRegions().get(i).getDistricts().get(j).getTownsDistrict().get(k).isCapital()){
                        capital =  state.getRegions().get(i).getDistricts().get(j).getTownsDistrict().get(k);
                    }
                }

            }

        }
        return capital;
    }

    @Override
    public int numberRegion(State state) {
        int number = state.getRegions().size();
        return number;
    }

    @Override
    public double areaState(State state) {
        double area = 0;
        for (int i = 0; i <state.getRegions().size() ; i++) {
            for (int j = 0; j <state.getRegions().get(i).getDistricts().size() ; j++) {
                area = area + state.getRegions().get(i).getDistricts().get(j).getAreaDistrict();
            }
        }
        return area;
    }

    @Override
    public List<Town> townsRegionsState(State state) {
        List <Town> townsRegions = new ArrayList <>();
        for (int i = 0; i <state.getRegions().size() ; i++) {
            for (int j = 0; j <state.getRegions().get(i).getDistricts().size() ; j++) {
                for (int k = 0; k <state.getRegions().get(i).getDistricts().get(j).getTownsDistrict().size() ; k++) {
                    if (state.getRegions().get(i).getDistricts().get(j).getTownsDistrict().get(k).isRegionalCenter()){
                        townsRegions.add(state.getRegions().get(i).getDistricts().get(j).getTownsDistrict().get(k));
                    }
                }

            }

        }
        return townsRegions;
    }
}
