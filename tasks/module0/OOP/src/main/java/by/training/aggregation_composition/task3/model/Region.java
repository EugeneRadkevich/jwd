package by.training.aggregation_composition.task3.model;

import java.util.List;

public class Region {
    List<District> districts;
    String nameRegion;

    public Region(List<District> districts, String nameRegion) {
        this.districts = districts;
        this.nameRegion = nameRegion;
    }

    public List<District> getDistricts() {
        return districts;
    }

    public void setDistricts(List<District> districts) {
        this.districts = districts;
    }

    public String getNameRegion() {
        return nameRegion;
    }

    public void setNameRegion(String nameRegion) {
        this.nameRegion = nameRegion;
    }

    @Override
    public String toString() {
        return "Region{" +
                "districts=" + districts +
                ", nameRegion='" + nameRegion + '\'' +
                '}';
    }
}
