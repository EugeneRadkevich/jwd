package by.training.oop_basics.task2;

import java.util.List;

public class Payment {

    public Payment() {
    }

    public void start () {
        System.out.println("Начинаем оплату");
    }

    public static class Transaction {
        List <Product> products;



        public Transaction(List<Product> products) {
            this.products = products;
        }

        public void execution(List<Product> products) {
            System.out.println("Производится оплата следующих товаров: " + products);
        }

    }
}
