package by.training.simple_objects.task7.service;

import by.training.simple_objects.task7.model.Point;
import by.training.simple_objects.task7.model.Triangle;

import java.util.List;

public interface TriangleService {

    double area (Triangle triangle);
    double perimeter (Triangle triangle);
    Point meridianIntersection (Triangle triangle);


}
