package by.training.oop_basics.task1;

public class TextFile extends File {

    @Override
    public boolean createFile (Directory dir, String name) {
        super.createFile(dir, name);
        System.out.println("Text file created");
        return true;
    }

    public boolean createFile(Directory dir) {
      return false;
    }


    @Override
    public boolean deleteFile(File file) {
        //super.deleteFile(file);
        System.out.println("Text file deleted");
        return true;
    }

    @Override
    public boolean renameFile(File file) {
        super.renameFile(file);
        System.out.println("Text file renamed");
        return true;
    }

    @Override
    public boolean updateFile(File file) {
        super.updateFile(file);
        System.out.println("Text file updated");
        return true;
    }

    @Override
    public void outPut(File file) {
        super.outPut(file);
        System.out.println("Text file output");

    }
}
