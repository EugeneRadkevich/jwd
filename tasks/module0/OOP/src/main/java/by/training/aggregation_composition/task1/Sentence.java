package by.training.aggregation_composition.task1;

import java.util.ArrayList;
import java.util.List;

public class Sentence {
    List<Word> contentSent;

    public Sentence() {
    }

    public Sentence(List<Word> contentSent) {
        this.contentSent = contentSent;
    }

    public List<Word> getContentSent() {
        return contentSent;
    }

    public void setContentSent(List<Word> contentSent) {
        this.contentSent = contentSent;
    }

    public void printSent (Sentence sentence) {
        for (int i = 0; i <sentence.contentSent.size() ; i++) {

            System.out.println(contentSent.get(i));
        }
    }

    @Override
    public String toString() {
        Sentence sentence = new Sentence(contentSent);
        String sent = "";
        for (int i = 0; i <sentence.contentSent.size() ; i++) {
            sent = sent + String.valueOf(contentSent.get(i));
        }

        return sent;
    }
}
